FROM webdevops/php:8.2-alpine

ARG M1

ENV WEB_DOCUMENT_ROOT=${WEB_DOCUMENT_ROOT:-/app/public} \
    APP_ENV=${APP_ENV:-production} \
    PHP_DISMOD=${PHP_DISMOD:-ioncube,ldap,soap,imap} \
    TZ=${TZ:-Asia/Kuala_Lumpur}

RUN mkdir -p ${WEB_DOCUMENT_ROOT:-/app/public}
WORKDIR ${WORKDIR:-/app}

ENV ENV=/etc/profile
COPY aliases.sh /etc/profile.d/aliases.sh

## Install go-replace. Required for M1 only
RUN if [ "${M1}" = 'true' ]; then \
    wget -O "/usr/local/bin/go-replace" "https://github.com/webdevops/go-replace/releases/download/22.10.0/go-replace.linux.arm64" \
    && chmod +x "/usr/local/bin/go-replace" \
    ; \
fi;

# some additional tool
RUN apk add --no-cache nano

# Install Laravel framework system requirements (https://laravel.com/docs/10.x/deployment#server-requirements)

# RUN composer install --no-interaction --optimize-autoloader --no-dev
# RUN php artisan config:cache
# RUN php artisan route:cache
# RUN php artisan view:cache
# RUN php artisan optimize

# Copy crontab application scheduler cron job
COPY crontab /etc/cron.d/application
RUN chown application:application /etc/cron.d/application \
    && chmod 0644 /etc/cron.d/application \
    && crontab -u application /etc/cron.d/application \
    && touch /var/log/cron.log

# copy application supervisord
COPY supervisord.conf /opt/docker/etc/supervisor.d/application.conf
RUN chmod 0644 /opt/docker/etc/supervisor.d/application.conf

RUN chown -R application:application .

# upgrade existing package for security issue
RUN apk upgrade --no-cache \
    && rm -rf /var/cache/apk/*
