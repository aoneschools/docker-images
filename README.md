# AOneSchools all docker images

## MongoDB server
- Folder `mongo-server`

## CI/CD
- Folder `CI-CD/`
- Build command
```bash
$ docker build --file php/8.2/Dockerfile.production -t php-fpm-82-alpine:latest .
```
